require "test_helper"

class Api::MkdirTest < ActionDispatch::IntegrationTest
  include ApiHelpers

  test "remove directory" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.rmdir(path)
      raise "wrong directory path #{path}" if "foo/bar/My-Dir" != path

      true
    end

    delete("/remove/prometheus/foo/bar/My-Dir?is_dir=true", headers:)

    assert_response :success
  end

  test "remove file" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.rm(path)
      raise "wrong directory path #{path}" if "foo/bar/file.txt" != path

      true
    end

    delete("/remove/prometheus/foo/bar/file.txt", headers:)
    assert_response :success
  end

  test "return 400 when removing failed" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.rm(path)
      raise "wrong directory path #{path}" if "foo/bar/file.txt" != path

      false
    end

    delete("/remove/prometheus/foo/bar/file.txt", headers:)
    assert_response :bad_request
  end
end
