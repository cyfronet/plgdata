require "test_helper"

class Api::DownloadTest < ActionDispatch::IntegrationTest
  include ApiHelpers

  test "download file content" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.get(path, before_first_chunk:, &block)
      if path == "net/scratch/people/marek/lorem-ipsum.txt"
        before_first_chunk.call
        block.call("What is ")
        block.call("Lorem Ipsum?")
      else
        raise "Wrong file #{path} to download"
      end
    end

    get("/download/prometheus/net/scratch/people/marek/lorem-ipsum.txt", headers:)

    assert_response :success
    assert_equal "What is Lorem Ipsum?", @response.body
  end

  test "download non existing file" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.get(path, before_first_chunk:, &block)
      if path == "non/existing/file"
        raise GFTP::GlobusError, "non existing"
      else
        raise "Wrong file #{path} to download"
      end
    end

    get("/download/prometheus/non/existing/file", headers:)

    assert_response 422
    assert_equal({ error: "non existing" }.to_json, @response.body)
  end
end
