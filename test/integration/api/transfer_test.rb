require "test_helper"

class Api::TransferTest < ActionDispatch::IntegrationTest
  include ApiHelpers

  test "transfers the file" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    pro.add("/net/scratch/people", Gftp::ClientMock::Dir.new(name: "marek"))
    pro.transferred = false
    def pro.transfer(source, destination, offset: nil)
      if source == "net/scratch/people/marek/to_copy" && source != destination
        self.transferred = true
      else
        raise "Unable to transfer files"
      end
    end

    put("/transfer/prometheus/net/scratch/people/marek/to_copy",
        params: { destination_path: "net/scratch/people/marek/copied" },
        headers:)
    assert_equal true, pro.transferred
  end
end
