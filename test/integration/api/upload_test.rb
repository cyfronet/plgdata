require "test_helper"

class Api::DownloadTest < ActionDispatch::IntegrationTest
  include ApiHelpers

  test "upload new file" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    pro.add("/net/scratch/people", Gftp::ClientMock::Dir.new(name: "marek"))

    pro.buffer = nil
    def pro.put(path, file, offset: nil)
      if path == "net/scratch/people/marek"
        self.buffer = file.read
      else
        raise "Wrong file name"
      end
    end

    post("/upload/prometheus/net/scratch/people/marek",
      params: { file: fixture_file_upload("foo.txt", "text/plain") },
      headers:)

    assert_equal pro.buffer, file_fixture("foo.txt").read
  end

  test "upload with recursive dir create" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    pro.add("/net/scratch/people", Gftp::ClientMock::Dir.new(name: "marek"))

    def pro.mkdir(path, dir_name, recursive: false)
      raise "wrong path #{path}" if "net/scratch/people/marek" != path
      raise "wrong dir name #{dir_name}" if "foo" != dir_name

      add(path, Gftp::ClientMock::Dir.new(name: dir_name))
    end

    pro.buffer = nil
    def pro.put(path, file, offset: nil)
      if path == "net/scratch/people/marek/foo"
        self.buffer = file.read
      else
        raise "Wrong file name"
      end
    end

    post("/upload/prometheus/net/scratch/people/marek/foo",
      params: { file: fixture_file_upload("foo.txt", "text/plain"), recursive: true },
      headers:)

    assert_equal pro.buffer, file_fixture("foo.txt").read
  end
end
