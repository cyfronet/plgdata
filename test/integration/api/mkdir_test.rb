require "test_helper"

class Api::MkdirTest < ActionDispatch::IntegrationTest
  include ApiHelpers

  test "create new directory" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.mkdir(path, dir_name, recursive: false)
      raise "wrong path #{path}" if "net/scratch/people/marek" != path
      raise "wrong dir name #{dir_name}" if  "My-New-Dir" != dir_name
      raise "should not be recursive" if recursive

      true
    end

    post("/mkdir/prometheus/net/scratch/people/marek/My-New-Dir", headers:)
    assert_response :success
  end

  test "create new directory recursive" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.mkdir(path, dir_name, recursive: false)
      raise "wrong path #{path}" if "net/scratch/people/marek" != path
      raise "wrong dir name #{dir_name}" if  "My-New-Dir" != dir_name
      raise "should not be recursive" unless recursive

      true
    end

    post("/mkdir/prometheus/net/scratch/people/marek/My-New-Dir",
      params: { recursive: true }, headers:)

    assert_response :success
  end


  test "return 422 when error occurs" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    def pro.mkdir(path, dir_name, recursive: false)
      false
    end

    post("/mkdir/prometheus/net/scratch/people/marek/My-New-Dir", headers:)

    assert_response :unprocessable_content
  end
end
