require "test_helper"

class Api::ListTest < ActionDispatch::IntegrationTest
  include ApiHelpers

  test "list directory entries" do
    pro = register_site_for(sites("prometheus"), proxy: "abc")

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::Dir.new(name: "MyDirectory"))
    pro.add("/net/scratch/people/marek", Gftp::ClientMock::File.new(name: "file.txt"))

    get("/list/prometheus/net/scratch/people/marek", headers:)

    assert_response :success
    list = response_json

    assert_equal "MyDirectory", list[0]["name"]
    assert list[0]["is_dir"]

    assert_equal "file.txt", list[1]["name"]
    assert_not list[1]["is_dir"]
  end

  test "list not existing site" do
    site_key = "not_existing_site"

    get("/list/#{site_key}/net/scratch/people/marek", headers:)

    assert_response 404
    assert_equal I18n.t("site_not_found", site: site_key), response_json["error"]
  end

  test "list not existing site translation by param" do
    site_key = "not_existing_site"
    get("/list/#{site_key}/net?locale=en", headers:)

    assert_response 404
    assert_equal "Could't find \"#{site_key}\" site", response_json["error"]
  end

  test "list not existing site translation by headers" do
    site_key = "not_existing_site"
    get "/list/#{site_key}/net", headers: headers.merge("HTTP_ACCEPT_LANGUAGE" => "pl")

    assert_response 404
    assert_equal "Nie znaleziono klastra \"#{site_key}\"", response_json["error"]
  end
end
