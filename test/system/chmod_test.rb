require "application_system_test_case"

class ChmodTest < ApplicationSystemTestCase
  test "Change directory rights" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek",
            Gftp::ClientMock::Dir.new(name: "MyDirectory", owner: "marek",
                                      rights: "rwxrwxrwx"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "Change access rights"
    fill_in "New access rights", with: "rwx------"
    click_on "Confirm access rights change"

    assert_no_text "rwxrwxrwx"
    assert_text "rwx------"
  end

  test "Change file rights" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek",
            Gftp::ClientMock::File.new(name: "foo.txt", owner: "marek",
                                       rights: "rwxrwxrwx"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "Change access rights"
    fill_in "New access rights", with: "rwxr--r--"
    click_on "Confirm access rights change"

    assert_no_text "rwxrwxrwx"
    assert_text "rwxr--r--"
  end
end
