require "application_system_test_case"

class ListTest < ApplicationSystemTestCase
  test "list directory entries" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::Dir.new(name: "MyDirectory"))
    pro.add("/net/scratch/people/marek", Gftp::ClientMock::File.new(name: "file.txt"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    assert_text "MyDirectory"
    assert_text "file.txt"
  end

  test "list root directory" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/", Gftp::ClientMock::Dir.new(name: "net"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "")

    assert_text "net"
  end

  test "switch to non existing site" do
    register_sites_for_user marek

    sign_in_as marek
    visit folder_path(site_key: "not-existing", path: "")

    assert_current_path root_path
    assert_text "Could't find \"not-existing\" site"
  end

  test "switch directory" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::Dir.new(name: "MyDirectory"))
    pro.add("/net/scratch/people/marek/MyDirectory", Gftp::ClientMock::Dir.new(name: "subdir"))
    pro.add("/net/scratch/people/marek/MyDirectory", Gftp::ClientMock::File.new(name: "file.txt"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "MyDirectory"

    assert_text "subdir"
    assert_text "file.txt"
  end

  test "switch to directory with polish letters" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::Dir.new(name: "Jacuś"))
    pro.add("/net/scratch/people/marek/Jacu%C5%9B", Gftp::ClientMock::File.new(name: "file.txt"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "Jacuś"

    assert_text "file.txt"
  end
end
