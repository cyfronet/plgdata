require "application_system_test_case"

class LoginTest < ApplicationSystemTestCase
  test "successfull login to all sites" do
    zeus, pro = register_sites_for_user marek

    zeus.add("/people", Gftp::ClientMock::Dir.new(name: "marek"))
    pro.add("/net/people", Gftp::ClientMock::Dir.new(name: "marek"))

    sign_in_as marek
    assert_text "Sign out"
    assert_text "Zeus"
    assert_text "Prometheus"
  end

  test "successfull login as a new user" do
    new_user = User.new(login: "new_user",
                        name: "New user", email: "new@user.local",
                        ssh_key: CredentialsProvider.key, ssh_certificate: CredentialsProvider.cert)
    zeus, pro = register_sites_for_user new_user

    zeus.add("/people", Gftp::ClientMock::Dir.new(name: "new_user"))
    pro.add("/net/people", Gftp::ClientMock::Dir.new(name: "new_user"))

    sign_in_as new_user
    assert_text "Sign out"
    assert_text "Zeus"
    assert_text "Prometheus"
  end

  test "does not show invalid sites" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/people", Gftp::ClientMock::Dir.new(name: "marek"))

    sign_in_as marek
    assert_text "Sign out"
    assert_no_text "Zeus"
    assert_text "Prometheus"
  end

  test "session is reset when ssh credentials are invalid" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/people", Gftp::ClientMock::Dir.new(name: "marek"))

    sign_in_as marek

    assert_text "Sign out"

    travel 2.hours do
      visit root_path
      assert_no_text "Sign out"
      assert_text "Sign in with"
    end
  end
end
