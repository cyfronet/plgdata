require "application_system_test_case"

class DeleteTest < ApplicationSystemTestCase
  test "remove directory" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::Dir.new(name: "MyDirectory"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    # We have only one element
    click_on "Delete"
    click_on "Yes"

    assert_text "File removed successfully"
    assert_no_text "MyDirectory"
  end

  test "remove file" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::File.new(name: "foo.txt"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    # We have only one element
    click_on "Delete"
    click_on "Yes"

    assert_text "File removed successfully"
    assert_no_text "foo.txt"
  end
end
