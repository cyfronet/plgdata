require "application_system_test_case"

class UploadTest < ApplicationSystemTestCase
  test "upload new file" do
    _zeus, pro = register_sites_for_user marek
    pro.add("/net/scratch/people", Gftp::ClientMock::Dir.new(name: "marek"))

    pro.buffer = nil
    def pro.put(path, file, offset: nil)
      if path == "net/scratch/people/marek"
        self.buffer = file.read
        add("/net/scratch/people/marek",
            Gftp::ClientMock::File.new(name: file.original_filename))
      else
        raise "Wrong file name"
      end
    end

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "Upload files"
    attach_file(file_fixture("foo.txt").to_path,
                class: "dz-hidden-input", make_visible: true)

    assert_text "foo.txt"
    assert_equal pro.buffer, file_fixture("foo.txt").read
  end

  test "upload new file in root location" do
    _zeus, pro = register_sites_for_user marek
    pro.add("/", Gftp::ClientMock::Dir.new(name: "net"))

    pro.buffer = nil
    def pro.put(path, file, offset: nil)
      if path == ""
        self.buffer = file.read
        add("/",
            Gftp::ClientMock::File.new(name: file.original_filename))
      else
        raise "Wrong file name"
      end
    end

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "")

    click_on "Upload files"
    attach_file(file_fixture("foo.txt").to_path,
                class: "dz-hidden-input", make_visible: true)

    assert_text "foo.txt"
    assert_equal pro.buffer, file_fixture("foo.txt").read
  end
end
