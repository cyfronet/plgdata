require "application_system_test_case"

class MkdirTest < ApplicationSystemTestCase
  test "create new directory" do
    _zeus, pro = register_sites_for_user marek
    pro.add("/net/scratch/people", Gftp::ClientMock::Dir.new(name: "marek"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "New directory"
    fill_in "Name the new directory", with: "My-New-Dir"
    click_on "Create"

    assert_text "My-New-Dir"
  end

  test "create new directory in root location" do
    _zeus, pro = register_sites_for_user marek
    pro.add("/", Gftp::ClientMock::Dir.new(name: "net"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "")

    click_on "New directory"
    fill_in "Name the new directory", with: "My-New-Dir"
    click_on "Create"

    assert_text "My-New-Dir"
  end
end
