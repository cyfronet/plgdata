require "application_system_test_case"

class DownloadTest < ApplicationSystemTestCase
  include DownloadHelpers

  test "download file content" do
    clear_downloads
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::File.new(name: "lorem-ipsum.txt"))

    def pro.get(path, before_first_chunk: nil, &block)
      if path == "net/scratch/people/marek/lorem-ipsum.txt"
        before_first_chunk.call
        block.call("What is ")
        block.call("Lorem Ipsum?")
      else
        raise "Wrong file #{path} to download"
      end
    end

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")
    click_on "lorem-ipsum.txt"

    assert_equal "What is Lorem Ipsum?", download_content
  end

  test "downloaded file name with space and polish letters" do
    clear_downloads
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek", Gftp::ClientMock::File.new(name: "ze spacją.txt"))

    def pro.get(path, before_first_chunk: nil, &block)
      if path == "net/scratch/people/marek/ze%20spacj%C4%85.txt"
        before_first_chunk.call
        block.call("Filename with space and polish letters")
      else
        raise "Wrong file #{path} to download"
      end
    end

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")
    click_on "ze spacją.txt"

    wait_for_download

    assert_equal "ze spacją.txt", File.basename(download)
  end
end
