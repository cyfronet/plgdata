require "application_system_test_case"

class RenameTest < ApplicationSystemTestCase
  test "Rename directory" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek",
            Gftp::ClientMock::Dir.new(name: "MyDirectory", owner: "marek"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "Change name"
    fill_in "New name", with: "MyNewDirectoryName"
    click_on "Confirm name change"

    assert_no_text "MyDirectory"
    assert_text "MyNewDirectoryName"
  end

  test "Rename file" do
    _zeus, pro = register_sites_for_user marek

    pro.add("/net/scratch/people/marek",
            Gftp::ClientMock::File.new(name: "foo.txt", owner: "marek"))

    sign_in_as marek
    visit folder_path(site_key: "prometheus", path: "net/scratch/people/marek")

    click_on "Change name"
    fill_in "New name", with: "bar.txt"
    click_on "Confirm name change"

    assert_no_text "foo.txt"
    assert_text "bar.txt"
  end
end
