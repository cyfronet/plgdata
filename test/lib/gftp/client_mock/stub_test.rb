require "test_helper"
require "gftp/client_mock"

class Gftp::ClientMock::StubTest < ActiveSupport::TestCase
  test "add dir creates empty entries table" do
    @stub = Gftp::ClientMock::Stub.new

    @stub.add("/a/b/c", Gftp::ClientMock::Dir.new(name: "dir"))

    assert_equal [], @stub.list("/a/b/c/dir")
  end

  test "entry exists" do
    @stub = Gftp::ClientMock::Stub.new

    @stub.add("/a/b/c", Gftp::ClientMock::File.new(name: "file"))

    assert @stub.exists?("/a/b/c/file")
    assert @stub.exists?("a/b/c/file")
    assert_not @stub.exists?("/a/b/c/dir")
  end

  test "list directory entries" do
    @stub = Gftp::ClientMock::Stub.new

    @stub.add("/a/b/c", Gftp::ClientMock::File.new(name: "file"))
    @stub.add("/a/b/c", Gftp::ClientMock::Dir.new(name: "dir"))
    @stub.add("/a/b/c/dir", Gftp::ClientMock::Dir.new(name: "subdir"))

    assert_equal [ "file", "dir" ], @stub.list("/a/b/c").map { |e| e[:name] }
    assert_equal [ "file", "dir" ], @stub.list("a/b/c").map { |e| e[:name] }
  end

  test "create new directory" do
    @stub = Gftp::ClientMock::Stub.new

    @stub.mkdir("/a/b/c", "new_dir")

    assert @stub.exists?("/a/b/c/new_dir")
  end

  test "remove directory" do
    @stub = Gftp::ClientMock::Stub.new
    @stub.add("/a/b/c", Gftp::ClientMock::Dir.new(name: "dir"))

    @stub.rmdir("/a/b/c/dir")

    assert_not @stub.exists?("/a/b/c/dir")
  end

  test "remove file" do
    @stub = Gftp::ClientMock::Stub.new
    @stub.add("/a/b/c", Gftp::ClientMock::File.new(name: "file"))

    @stub.rm("/a/b/c/file")

    assert_not @stub.exists?("/a/b/c/file")
  end

  test "rename file" do
    @stub = Gftp::ClientMock::Stub.new
    @stub.add("/a/b/c", Gftp::ClientMock::Dir.new(name: "dir"))

    @stub.rename("/a/b/c/dir", "new-dir-name")

    assert @stub.exists?("/a/b/c/new-dir-name")
    assert_not @stub.exists?("/a/b/c/dir")
  end

  test "mode file" do
    @stub = Gftp::ClientMock::Stub.new
    @stub.add("/a/b/c", Gftp::ClientMock::Dir.new(name: "dir"))

    @stub.move("/a/b/c/dir", "/aa/bb/cc/new_location")

    assert @stub.exists?("/aa/bb/cc/new_location")
    assert_not @stub.exists?("/a/b/c/dir")
  end

  test "change mode" do
    @stub = Gftp::ClientMock::Stub.new
    @stub.add("/a/b/c", Gftp::ClientMock::File.new(name: "file"))
    @stub.add("/a/b/c", Gftp::ClientMock::Dir.new(name: "dir"))

    @stub.chmod("/a/b/c/file", "rw-r-----")
    @stub.chmod("/a/b/c/dir", "rwx------")
    file = @stub.list("/a/b/c").find { |e| e[:name] == "file" }
    dir = @stub.list("/a/b/c").find { |e| e[:name] == "dir" }

    assert_equal "-rw-r-----", file[:rights]
    assert_equal "drwx------", dir[:rights]
  end
end
