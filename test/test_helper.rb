ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
require "webmock/minitest"

Dir[Rails.root.join("test/support/**/*.rb")].each { |f| require f }

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  def register_sites_for_user(user)
    register_sites_for(key: user.ssh_key, cert: user.ssh_certificate)
  end

  def register_sites_for(**)
    [ register_site_for(sites("zeus"), **), register_site_for(sites("prometheus"), **) ]
  end

  def register_site_for(site, **)
    Gftp::ClientMock::StubRegistry.instance
      .register_stub(host: site.host, **)
  end

  def reset_gftp_stubs!
    Gftp::ClientMock::StubRegistry.instance.reset!
  end

  def marek
    users("marek")
  end
end

WebMock.disable_net_connect!(allow_localhost: true,
                             allow: "chromedriver.storage.googleapis.com")
