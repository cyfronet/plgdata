require "test_helper"

OmniAuth.config.test_mode = true

ActiveSupport.on_load(:action_dispatch_system_test_case) do
  ActionDispatch::SystemTesting::Server.silence_puma = true
end

Capybara.enable_aria_label = true

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :headless_chrome, screen_size: [ 1400, 1400 ] do |opts|
    # Make possible to test file downloading. Based on:
    # https://github.com/renuo/so_many_devices/blob/master/lib/so_many_devices.rb
    opts.add_preference(:download, prompt_for_download: false,
                        default_directory: DownloadHelpers::PATH.to_s)

    opts.add_preference(:browser, set_download_behavior: { behavior: "allow" })

    opts.add_argument("--no-sandbox")
    opts.add_argument("--disable-dev-shm-usage")
  end

  def sign_in_as(user, key: user.ssh_key, cert: user.ssh_certificate)
    token = SecureRandom.base64
    OmniAuth.config.add_mock(
      :plgrid,
      uid: Random.uuid,
      credentials: { token: },
      info: { name: user.name, email: user.email, nickname: user.login },
      extra: { raw_info: { groups: user.teams&.values } },
      provider: "plgrid"
    )

    WebMock.stub_request(:post, "#{Plgrid::Ccm::CCM_URI}")
      .with(headers: { "Authorization" => "Bearer #{token}" })
      .to_return(status: 200, body: { cert:, private: key }.to_json)

    visit "/auth/plgrid"
  end
end
