module ApiHelpers
  def headers
    { "PROXY" => Base64.encode64("abc") }
  end

  def response_json
    JSON.parse(@response.body)
  end
end
