require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "update list of valid user sites" do
    marek = users("marek")
    _zeus, pro = register_sites_for(cert: marek.ssh_certificate, key: marek.ssh_key)

    pro.add("/net/people", Gftp::ClientMock::Dir.new(name: "marek"))

    marek.validate_user_sites!

    assert_equal 1, marek.sites.size
    assert_equal sites("prometheus"), marek.sites.first
  end
end
