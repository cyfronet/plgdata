require "test_helper"

class SiteClientTest < ActiveSupport::TestCase
  test "compose site links" do
    marek = users("marek")
    register_site_for(sites("prometheus"), key: marek.ssh_key, cert: marek.ssh_certificate)

    dirs = SiteClient.new(sites("prometheus"), marek).dirs

    assert_equal 4, dirs.size
    assert_equal "/net/people/marek/", dirs["Home"]
    assert_equal "/storage/path/marek/", dirs["Storage"]
    assert_equal "/net/archive/groups/my/", dirs["My personal team"]
    assert_equal "/net/archive/groups/dice/", dirs["DICE"]
  end
end
