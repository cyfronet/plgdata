# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2023_09_08_111150) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "site_dirs", force: :cascade do |t|
    t.jsonb "name", default: {}
    t.string "path", null: false
    t.bigint "site_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["site_id"], name: "index_site_dirs_on_site_id"
  end

  create_table "sites", force: :cascade do |t|
    t.jsonb "name", default: {}
    t.string "key", null: false
    t.string "host", null: false
    t.integer "port", null: false
    t.string "user_dir", null: false
    t.string "groups_dir", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_sites", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "site_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["site_id"], name: "index_user_sites_on_site_id"
    t.index ["user_id"], name: "index_user_sites_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "login"
    t.string "name"
    t.string "email"
    t.json "teams", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "ssh_key"
    t.text "ssh_certificate"
    t.index ["login"], name: "index_users_on_login", unique: true
  end

  add_foreign_key "site_dirs", "sites"
  add_foreign_key "user_sites", "sites"
  add_foreign_key "user_sites", "users"
end
