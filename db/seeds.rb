# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Site.destroy_all

Site.create!(key: "ares", name_pl: "Ares", name_en: "Ares",
             host: "login01.ares.cyfronet.pl", port: 2811,
             user_dir: "/net/people/plgrid/", groups_dir: "/net/pr2/projects/plgrid/")
