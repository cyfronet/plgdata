class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :login, nill: false
      t.string :name, nill: false
      t.string :email, nill: false
      t.text :proxy
      t.string :dn, nill: false
      t.json :teams, default: {}

      t.boolean :zeus_enabled, null: false, default: false
      t.boolean :prometheus_enabled, null: false, default: false

      t.timestamps
    end
    add_index :users, :login, unique: true
  end
end
