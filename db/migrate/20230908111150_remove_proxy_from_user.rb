class RemoveProxyFromUser < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :proxy, :text
    remove_column :users, :dn, :string
  end
end
