class CreateSiteDirs < ActiveRecord::Migration[7.0]
  def change
    create_table :site_dirs do |t|
      t.jsonb :name, default: {}
      t.string :path, null: false
      t.references :site, null: false, foreign_key: true

      t.timestamps
    end
  end
end
