class RemoveSiteEnabledFromUser < ActiveRecord::Migration[7.0]
  def change
    remove_column :users,  :zeus_enabled, null: false, default: false
    remove_column :users, :prometheus_enabled, null: false, default: false
  end
end
