class CreateSites < ActiveRecord::Migration[7.0]
  def change
    create_table :sites do |t|
      t.jsonb :name, default: {}
      t.string :key, null: false
      t.string :host, null: false
      t.integer :port, null: false
      t.string :user_dir, null: false
      t.string :groups_dir, null: false

      t.timestamps
    end
  end
end
