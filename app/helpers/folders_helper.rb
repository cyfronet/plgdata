module FoldersHelper
  def entry_path(entry)
    File.join(path, entry[:name])
  end

  #  Removes invalid characters from the entry name so it is safe for further processing
  def remove_invalid(name)
    # name.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '?')
    name
  end

  def owner_tooltip(entry)
    "#{t 'user'}: #{entry[:owner]}<br/>#{t 'group'}: #{entry[:group]}"
  end
end
