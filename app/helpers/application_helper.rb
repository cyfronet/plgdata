module ApplicationHelper
  # Shows a popup telling the user what are her attributes in the system.
  def user_info(user)
    user_teams = user.teams.keys.join(", ")

    content_tag(:strong, user.name, style: "display: block") +
      content_tag(:span, user.login, style: "display: block") +
      content_tag(:span, user.email, style: "display: block") +
      (user_teams.present? ?
        content_tag(:span, "#{t 'your_teams'}: [#{user_teams}]", style: "display: block") :
        "")
  end

  @@links = {
    cyfronet_pl: "http://www.cyfronet.pl",
    cyfronet_en: "http://www.cyfronet.pl/en",
    plgrid_pl: "http://www.plgrid.pl",
    plgrid_en: "http://www.plgrid.pl/en",
    helpdesk_pl: "https://helpdesk.plgrid.pl/",
    helpdesk_en: "https://helpdesk.plgrid.pl/",
    portal_pl: "https://portal.plgrid.pl/",
    portal_en: "https://portal.plgrid.pl/",
    registration_pl: "https://portal.plgrid.pl/web/guest/home?p_p_id=arulogin_WAR_aruliferay&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=4&_arulogin_WAR_aruliferay_action=register",
    registration_en: "https://portal.plgrid.pl/web/guest/home?p_p_id=arulogin_WAR_aruliferay&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=4&_arulogin_WAR_aruliferay_action=register",
    plgrid_manual_pl: "https://docs.plgrid.pl/pages/viewpage.action?pageId=4260611",
    plgrid_manual_en: "https://docs.plgrid.pl/display/PLGDoc/User+manual"
  }

  def footer_link(link_name)
    @@links[link_name] ? @@links[link_name.to_sym] : @@links["#{link_name}_#{I18n.locale}".to_sym]
  end
end
