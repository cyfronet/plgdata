// Entry point for the build script in your package.json
import "@hotwired/turbo-rails"
import "./controllers"
import { createPopper } from '@popperjs/core';
import * as bootstrap from "bootstrap"

Turbo.session.drive = false

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

