import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["button", "content"]

  switch(event) {
    this.switchButtonTo(event.currentTarget);
    this.switchContentTo(event.currentTarget.dataset.tabContent);
  }

  switchButtonTo(button) {
    this.buttonTargets.forEach((target) => {
      // console.log("target %o", target);
      // console.log("button %o", button);
      if(target == button) {
        target.classList.toggle("active");
      } else {
        target.classList.remove("active");
      }
    });
  }

  switchContentTo(contentName) {
    this.contentTargets.forEach((content) => {
      if(content.dataset.tabContent == contentName) {
        content.classList.toggle("d-none");
      } else {
        content.classList.add("d-none");
      }
    });
  }
}
