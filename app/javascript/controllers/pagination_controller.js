import { Controller } from "@hotwired/stimulus";
import { get } from "@rails/request.js"

export default class extends Controller {
  static values = { nextUrl: String };

  connect() {
    this.observer = new IntersectionObserver((entries) => {
        if(entries[0].isIntersecting){
          this._fetchNewPage()
          this.observer.disconnect();
        }
    });
    this.observer.observe(this.element);
  }

  disconnect() {
    this.observer.disconnect();
  }

  async _fetchNewPage() {
    get(this.nextUrlValue, { responseKind: "turbo-stream" })
  }
}
