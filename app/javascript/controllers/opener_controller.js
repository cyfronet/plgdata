import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="opener"
export default class extends Controller {
  static targets = ["part"]
  static values = {
    timeout: { type: Number, default: 2000 }
  }

  show() {
    this.partTargets.forEach(element => {
        element.classList.add(element.dataset.openerAddClass)
        element.classList.remove(element.dataset.openerRemoveClass)
    })

    clearTimeout(this.closer)
    this.closer = setTimeout(() => { this.close() }, this.timeoutValue)
  }

  close() {
    this.partTargets.forEach(element => {
        element.classList.add(element.dataset.openerRemoveClass)
        element.classList.remove(element.dataset.openerAddClass)
    })
  }
}
