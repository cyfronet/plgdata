import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static values = { placeholder: String, regex: String }

  connect() {
    this.previousInputValue = this.element.value;
  }

  onChange(event) {
    event.stopPropagation();

    const position = this.position;
    const candidate = this.candidate(this.previous, event.target.value, position);

    if (this.regexp.test(candidate)) {
      this.value = candidate;
      this.element.setSelectionRange(position, position);
    } else {
      this.value = this.previous;
      this.element.setSelectionRange(position - 1, position);
    }
  }

  candidate(prev, current, position) {
    let currentArray = [...current];
    let diff = current.length - prev.length;

    if(diff > 0) {
      currentArray.splice(position + diff - 1, diff);
    }
    else if(diff < 0) {
      currentArray.splice(position, 0, this.placeholderValue.slice(position, position - diff))
    }
    currentArray = currentArray.slice(0, this.placeholderValue.length)

    return currentArray.join("");
  }

  set value(value) {
    this.previousInputValue = value;
    this.element.value = value;
  }

  get previous() {
    return this.previousInputValue;
  }

  get position() {
    return this.element.selectionStart;
  }

  get regexp() {
    return new RegExp(this.regexValue);
  }
}
