import { Controller } from "@hotwired/stimulus";
import Dropzone from "dropzone"

Dropzone.autoDiscover = false

export default class extends Controller {
  connect() {
    new Dropzone(this.element, { paramName: "file",
      chunking: true, retryChunks: true, parallelChunkUploads: true,
      parallelUploads: 4, maxFilesize: null })
      .on("queuecomplete", function(file) {
        window.location.reload(true);
      });
  }
}
