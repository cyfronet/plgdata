require "socket"
require "timeout"

class Site < ApplicationRecord
  extend Mobility
  translates :name

  has_many :site_dirs, dependent: :destroy
  has_many :user_sites, dependent: :destroy

  validates :name_pl, presence: true
  validates :name_en, presence: true
  validates :key, presence: true
  validates :host, presence: true
  validates :port, presence: true
  validates :user_dir, presence: true
  validates :groups_dir, presence: true

  def test
    Timeout.timeout(1) do
      s = TCPSocket.new(host, port)
      s.close
      return [ true, nil ]
    end

    # Should not reach that point
    [ false, "#{name}: Unknown error" ]
  rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH, SocketError, Timeout::Error => error
    [ false, "#{name}: #{error.message}" ]
  end
end
