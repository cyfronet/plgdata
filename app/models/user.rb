class User < ApplicationRecord
  TECHNICAL_GROUP_PREFIX = "plgg-"

  has_many :user_sites, dependent: :destroy
  has_many :sites, through: :user_sites

  def validate_user_sites!
    user_sites.destroy_all

    Site.find_each do |site|
      user_sites.create(site:) if site_client(site).valid?
    end
  end

  def site_clients
    @site_clients ||= sites.sort_by(&:name).map { |site| site_client(site) }
  end

  def site_client(site)
    SiteClient.new(site, self)
  end

  def credentials_valid?
    ssh_credentials.valid?
  end

  private
    def ssh_credentials
      GFTP::SshCredentials.new(ssh_key, ssh_certificate)
    end
end
