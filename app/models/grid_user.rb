require "gftp/client"

class GridUser
  CLIENT_CLASS = Plgdata::Application.config.gftp_client_class.constantize

  def initialize(proxy:)
    @proxy = proxy
  end

  def site_client(site)
    CLIENT_CLASS.grid(host: site.host, proxy: @proxy)
  end
end
