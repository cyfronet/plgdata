class SiteDir < ApplicationRecord
  extend Mobility
  translates :name

  belongs_to :site

  validates :name_pl, presence: true
  validates :name_en, presence: true
  validates :path, presence: true
end
