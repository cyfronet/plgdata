require "net/http"

class Plgrid::Ccm
  CCM_URI = URI.parse(Rails.application.credentials.plgrid.dig(:ccm, :uri))

  attr_reader :certificate, :key

  def initialize(token, read_timeout: 2)
    @token = token
    @read_timeout = read_timeout
  end

  def fetch!
    response = Net::HTTP.start(CCM_URI.hostname, CCM_URI.port,
                               use_ssl: CCM_URI.scheme == "https",
                               read_timeout: @read_timeout) do |http|
      http.post(CCM_URI, "", "Authorization" => "Bearer #{@token}")
    end

    case response
    in Net::HTTPOK => response
      json_response = JSON.parse(response.body)
      @certificate = json_response["cert"]
      @key = json_response["private"]
    in _ => response
      Rails.logger.warn <<~ERROR
        Error while fetching short lived ssh key with correct user token
          - status code: #{response.code}
          - error body: #{response.body}
      ERROR

      raise "Cannot fetch short lived ssh key"
    end
  end
end
