require "gftp/client"

class SiteClient
  CLIENT_CLASS = Plgdata::Application.config.gftp_client_class.constantize

  def initialize(site, user)
    @site = site
    @user = user

    @client = CLIENT_CLASS.ssh(host: site.host,
                               cert: user.ssh_certificate, key: user.ssh_key)
  end

  delegate :exists?, :list, :mkdir, :rmdir, :get, :put, :rm, :rename, :move, :chmod, :transfer,
    to: :@client

  delegate :name, :key, to: :@site

  def dirs
    { "Home" => home_dir }
      .merge(user_dirs)
      .merge(groups_dirs)
  end

  def valid?
    @user.login && exists?(home_dir)
  rescue
    false
  end

  private
    def home_dir
      File.join(@site.user_dir, @user.login, "/")
    end

    def user_dirs
      @site.site_dirs.to_h { |sd| [ sd.name, File.join(sd.path, @user.login, "/") ] }
    end

    def groups_dirs
      @user.teams.to_h { |key, name| [ name, File.join(@site.groups_dir, key, "/") ] }
    end
end
