class Current < ActiveSupport::CurrentAttributes
  attribute :user, :site, :path, :client

  def user=(user)
    super
    initilize_site_client(user, site)
  end

  def site=(site)
    super
    initilize_site_client(user, site)
  end

  def path=(path)
    super begin
      parser = URI::Parser.new
      path = parser.escape(parser.unescape(path))

      path.delete_prefix("/")
    end
  end

  def site_key
    site&.key
  end

  def login
    user&.login
  end

  private
    def initilize_site_client(user, site)
      self.client = user.site_client(site) if user && site
    end
end
