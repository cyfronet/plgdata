class MonitoringController < ApplicationController
  skip_before_action :verify_authenticity_token


  def health_data
    if authenticated?
      service_status = "OK"
      error_messages = []

      Site.all.each do |site|
        ok, error = site.test

        unless ok
          service_status = "ERROR"
          error_messages << error
        end
      end

      begin
        cnt = User.count.to_i
        logger.info "Testing DB connection. Current user count is #{cnt}."
      rescue ActiveRecord::StatementInvalid => error
        error_messages << error.message
        service_status = "ERROR"
      end

      current_time = Time.now.strftime("%d/%m/%Y %H:%M:%S")
      service_info = <<-EOF
        <healthdata date="#{current_time}" status="#{service_status}" message="#{error_messages.join(";")}">
        </healthdata>
      EOF

      respond_to do |format|
        format.xml { render inline: service_info }
      end
    else
      render inline: "No proper nagios_secret_token available", status: 403
    end
  end

  private
    def authenticated?
      params[:nagios_secret_token].present? &&
        Plgdata::Application.config.nagios_secret_token.present? &&
        params[:nagios_secret_token] == Plgdata::Application.config.nagios_secret_token
    end
end
