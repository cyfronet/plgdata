class Api::FoldersController < Api::ApplicationController
  def index
    entries = Current.client.list(path)

    render json: entries
  end

  def create
    dir_path = File.dirname(path)
    dir_name = File.basename(path)

    result = Current.client.mkdir(dir_path, dir_name, recursive: params[:recursive] == "true")

    logger.info "Creating new directory: #{path}/#{dir_name} -> #{result}"
    result ? head(:ok) : render(status: 422, text: I18n.t("folder.error"))
  end
end
