class Api::FilesController < Api::ApplicationController
  include FileStreaming

  # GET /download/:path
  def download
    stream_file path
  end

  # POST /upload/:path
  # Requires :file multiform param, with the file object uploaded there
  def upload
    if params["recursive"] == "true" && !Current.client.mkdir(dir_path, dir_name, recursive: true)
      render(status: 422, json: { text: I18n.t("folder.error") })
      return
    end

    unless params[:file].instance_of?(ActionDispatch::Http::UploadedFile)
      render(status: 422, json: { text: I18n.t("upload.wrong_parameter") })
      return
    end

    target_filename = Current.client.put(path, params[:file])

    render json: { name: target_filename, notice: I18n.t("upload.success") }
  end

  # PUT /transfer/:site/:source_path
  # Requires :destination_path
  def transfer
    success = Current.client.transfer(params[:source_path], params[:destination_path])

    if success
      render json: { name: @source, notice: I18n.t("transfer.success") }
    else
      render status: 400, json: { name: @source, error: I18n.t("transfer.error") }
    end
  end

  # DELETE /remove/:path
  # You need to pass the additional is_dir=true parameter if you intend
  # to remove a folder
  def remove
    success =
      params[:is_dir] == "true" ? Current.client.rmdir(path) : Current.client.rm(path)

    if success
      render json: { name: path, notice: I18n.t("delete.success") }
    else
      render status: 400, json: { name: path, error: I18n.t("delete.error") }
    end
  end

  private
    def dir_name
      File.basename(path)
    end

    def dir_path
      File.dirname(path)
    end
end
