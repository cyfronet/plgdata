# Not secured through usual authentication mechanism:
# relies on proxy sent directly through an SSL link
class Api::ApplicationController < ActionController::API
  include SetGftp, Api::Authenticate, Api::SetLocale

  rescue_from GFTP::GlobusError do |exception|
    logger.warn exception
    render status: 422, json: { error: exception.message }
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    logger.warn exception
    render status: 404, json: { error: I18n.t("site_not_found", site: params[:site_key]) }
  end
end
