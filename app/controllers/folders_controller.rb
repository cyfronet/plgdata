class FoldersController < ApplicationController
  before_action :authenticate_user!

  def show
    @entries = Current.client.list(path)

    @hidden_entries = @entries.select { |e| e[:name].start_with?(".") }.size
    if (params[:hide_hidden] == "true") || (cookies[:hide_hidden] && (params[:hide_hidden] != "false"))
      @entries = @entries.reject { |e| e[:name].start_with?(".") }
      cookies[:hide_hidden] = true if params[:hide_hidden]
    end
    cookies.delete :hide_hidden if params[:hide_hidden] == "false"

    @more_entries = @entries.size > page * page_size
    @entries = @entries[interval] || []

    respond_to do |format|
      format.html
      format.turbo_stream
    end
  end

  def create
    result = params[:folder_name].present? &&
      Current.client.mkdir(path, params[:folder_name])

    redirect_to unescape(folder_path(site_key:, path:)),
      notice: result ? I18n.t("folder.success") : nil,
      status: :see_other
  end

  def no_access_error
  end

  private
    def interval
      ((page - 1) * page_size)...(page * page_size)
    end

    def page
      @page ||= params[:page] ? params[:page].to_i : 1
    end

    def page_size
      Plgdata::Application.config.items_per_page || 1000
    end
end
