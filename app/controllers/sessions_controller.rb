class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create

  def create
    user = User.find_or_initialize_by(login: auth.info.nickname).tap do |u|
      u.login = auth.info.nickname
      u.email = auth.info.email
      u.name = auth.info.name
      u.teams = teams
      u.ssh_key, u.ssh_certificate = fetch_short_lived_ssh_credentials!

      u.save!


      u.validate_user_sites!
    end
    session[:user_id] = user.id

    redirect_to post_authenticating_url, notice: t("authenticated")
  end

  def destroy
    Current.user&.update(ssh_key: nil, ssh_certificate: nil)
    session[:user_id] = nil

    redirect_to root_path
  end

  private
    def auth
      request.env["omniauth.auth"]
    end

    def fetch_short_lived_ssh_credentials!
      ccm = Plgrid::Ccm.new(token)
      ccm.fetch!

      [ ccm.key, ccm.certificate ]
    end

    def token
      auth.dig("credentials", "token")
    end

    def teams
      auth.dig("extra", "raw_info", "groups").to_h { |i| [ i, i ] }
    end
end
