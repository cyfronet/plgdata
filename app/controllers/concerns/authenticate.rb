module Authenticate
  extend ActiveSupport::Concern

  included do
    prepend_before_action do
      Current.user = User.find_by(id: session[:user_id]) if session[:user_id]
      unless Current.user&.credentials_valid?
        session[:user_id] = nil
        Current.user = nil
      end
    end
  end

  private
    def authenticate_user!
      session[:return_to_after_authenticating] = request.url

      redirect_to root_path, alert: t("login_required") unless Current.user
    end

    def post_authenticating_url
      session.delete(:return_to_after_authenticating) || root_url
    end
end
