module FileStreaming
  extend ActiveSupport::Concern

  included do
    include ActionController::Live
  end

  private
    def stream_file(path)
      size = 0
      Current.client.get(path, before_first_chunk: -> { set_streaming_headers(path) }) do |data|
        size += data.size
        response.stream.write data
      end
      logger.info "Total downloaded bytes: #{size}."
    rescue GFTP::GlobusError => e
      logger.warn e
      headers["Content-Type"] = Mime::Type.lookup_by_extension("json")
      response.stream.write({ error: e.message }.to_json)
      response.status = 422
    ensure
      response.stream.close
    end

    def set_streaming_headers(path)
      filename = URI::Parser.new.unescape(File.basename(path))
      headers["Content-Type"] =
        Mime::Type.lookup_by_extension(File.extname(filename)
          .downcase.delete(".")) || "application/octet-stream"

      headers["Content-Disposition"] =
        ActionDispatch::Http::ContentDisposition
          .format(disposition: :attachement, filename:)

      # Turn off ETag generation
      headers["Last-Modified"] = "0"
      headers["ETag"] = "0"

      # This is to switch Nginx buffering off for this particular response
      response.headers["X-Accel-Buffering"] = "no"
    end
end
