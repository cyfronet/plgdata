module SetGftp
  extend ActiveSupport::Concern

  included do
    before_action do
      params[:path] ||= ""

      Current.site = Site.find_by! key: params[:site_key] if params[:site_key]
      Current.path = params[:path] if params[:path]
    end

    delegate :site_key, :path, to: :Current
  end

  private
    def unescape(path)
      URI::Parser.new.unescape(path)
    end
end
