module Api::SetLocale
  extend ActiveSupport::Concern

  included do
    prepend_before_action :set_locale
  end

  private
    def set_locale
      I18n.locale = new_locale
    end

    def new_locale
      locale = params[:locale] || locale_from_accept_language_header
      valid_locale(locale)
    end

    def locale_from_accept_language_header
      request.
        env["HTTP_ACCEPT_LANGUAGE"].
        scan(/^[a-z]{2}/).first rescue I18n.default_locale
    end

    def valid_locale(locale)
      valid_locale?(locale) ? locale : I18n.default_locale
    end

    def valid_locale?(locale)
      I18n.available_locales.map(&:to_s).include?(locale)
    end
end
