module Api::Authenticate
  extend ActiveSupport::Concern

  included do
    prepend_before_action do
      Current.user = GridUser.new(proxy:) if proxy
      Current.user = User.new(ssh_key: key, ssh_certificate: cert) if key && cert

      authenticate_user!
    end
  end

  private
    def authenticate_user!
      render status: 401, json: { error: "No credentials provided." } unless Current.user
    end

    def proxy
      @proxy ||= credential(:proxy)
    end

    def cert
      @cert ||= credential(:sshcert)
    end

    def key
      @key ||= credential(:sshkey)
    end

    def credential(key)
      params[key] ||
        request.headers[key.to_s.upcase] &&
        Base64.decode64(request.headers[key.to_s.upcase])
    end
end
