class FilesController < ApplicationController
  include FileStreaming

  before_action :authenticate_user!

  # GET /files/:path
  def show
    stream_file path
  end

  # Supports both straight upload or chunked upload version version
  def create
    file = params[:file]
    if file
      Current.client.put(path, file, offset:)

      head :no_content
    else
      render plain: I18n.t("upload.wrong_parameter"), status: :bad_request
    end
  rescue GFTP::GlobusError => error
    logger.warn error
    error_message = (error.message.include?("500") ? error.message.split("500")[-2] : error.message)
    error_message = "#{I18n.t("upload.error")}: #{error_message}"
    render plain: error_message, status: :bad_request
  end

  def offset
    params[:dzchunkbyteoffset]&.to_i
  end

  # NOTE: currently used for chmod and rename operations; in the future possibly also useful for move?
  def update
    if params[:mode]
      @notice = success_message("chmod") if Current.client.chmod(path, params[:mode])
    end

    if params[:destination] && params[:destination].strip.present?
      @notice = success_message("rename") if Current.client.rename(path, params[:destination])
    end

    redirect_to unescape(folder_path(path: path.rpartition("/").first)),
      notice: @notice, status: :see_other
  end

  def success_message(prefix)
    t("#{prefix}.success_#{params[:is_dir] == 'true' ? 'folder' : 'file'}")
  end


  def delete
    result = dir? ? Current.client.rmdir(path) : Current.client.rm(path)

    redirect_to unescape(folder_path(site_key:, path: path.rpartition("/").first)),
      notice: result ? I18n.t("delete.success") : nil,
      status: 303
  end

  private
    def dir?
      params[:is_dir] == "true"
    end
end
