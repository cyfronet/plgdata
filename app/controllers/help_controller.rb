class HelpController < ApplicationController
  HELP_SECTIONS = %w[api]

  def show
    category = params[:category].presence_in(HELP_SECTIONS)

    if category
      render category
    else
      redirect_to help_path(HELP_SECTION.first)
    end
  end
end
