class ApplicationController < ActionController::Base
  allow_browser versions: :modern

  include SetLocale, Authenticate, SetGftp

  helper_method :site_key, :path

  rescue_from GFTP::GlobusError do |exception|
    logger.warn exception
    redirect_back fallback_location: no_access_error_path,
                  alert: exception.message
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    logger.warn exception
    redirect_back fallback_location: root_path,
                  alert: I18n.t("site_not_found", site: params[:site_key])
  end
end
