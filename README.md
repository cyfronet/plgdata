# PLData - A simple tool for PL-Grid files management

PLGData allows you to access the folders and files that are available for you
in the PL-Grid Infrastructure. These include both your personal resources and
also those shared with you (or the teams you belong to) by collaborators.

## Dependencies

[asdf-vm](https://asdf-vm.com) can be used to install dependencies below

* MRI (`asdf plugin add ruby`)

Then run `asdf install`

* PostgreSQL (`sudo apt-get install postgresql`)
* PostgreSQL libpq-dev (`sudo apt-get install libpq-dev`)
* Yarn ([installation guide](https://classic.yarnpkg.com/en/docs/install#debian-stable))
* GridFTP (`sudo apt install libglobus-ftp-client-dev`)

Copy contents of [this file](https://gitlab.com/cyfronet/plgdata/-/blob/main/ansible/roles/grid_ftp/templates/gridftp-ssh.j2?ref_type=heads) to `~/.globus/gridftp-ssh` and change the permissions
```
chmod 755 ~/.globus/gridftp-ssh
```

## DBMS Settings

You need to create user/role for your account in PostgreSQL. You can do it
using the 'createuser' command line tool. Then, make sure to alter this user
for rights to create databases.

## Running in development mode

You need to have `config/master.key` file to run an application in development
mode. Contact @mkasztelnik for details.

To start only web application run:

```
yarn install
./bin/setup
bin/dev
```

Now you can point your browser into `https://localhost:3000`.

## ENV variables

To customize the application you can set the following ENV variables:
  * `PLGDATA_NAGIOS_SECRET` - if present nagios health monitoring is enabled
  * `SENTRY_DN` - if present PLGData will sent error and performance
    information to Sentry
  * `SENTRY_ENVIRONMENT` - if present overrides sentry environment (default set
    to `Rails.env`)

## Testing

Some tests require Chrome headless installed. Please take a look at:
https://developers.google.com/web/updates/2017/04/headless-chrome for manual. To
install chrome on your debian based machine use following snippet:

```
sudo su
curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list

apt-get update -q
apt-get install -y google-chrome-stable
exit
```

To execute all tests run:

```
./bin/rails test
./bin/rails test:system
./bin/rails test:all
```
## Contributing

1. Fork the project
1. Create your feature branch (`git checkout -b my-new-feature`)
1. Commit your changes (`git commit -am 'Add some feature'`)
1. Push to the branch (`git push origin my-new-feature`)
1. Create new pull request
1. When feature is ready add reviewers and wait for feedback (at least one
   approve should be given and all review comments should be resolved)
