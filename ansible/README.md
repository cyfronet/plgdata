# Install dependencies

```
ansible-galaxy collection install -r requirements.yml
```

# Run

```
ansible-playbook -i production site.yml
```

# Tips

To see application logs use:

```
journalctl --user-unit -u {{target}}

#e.g.
journalctl --user-unit -u PLGData-staging.target
```
