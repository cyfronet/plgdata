Rails.application.routes.draw do
  root "home#index"

  delete "logout", to: "sessions#destroy", as: "logout"
  match "auth/:provider/callback", to: "sessions#create", via: [ :get, :post ]
  get "auth/failure", to: redirect("/")

  # API
  get "download/:site_key/*path" => "api/files#download", constraints: { path: /.*/ }
  post "upload/:site_key/*path" => "api/files#upload", constraints: { path: /.*/ }
  delete "remove/:site_key/*path" => "api/files#remove", constraints: { path: /.*/ }
  put "/transfer/:site_key/*source_path", to: "api/files#transfer", constraints: { path: /.*/ }
  get "list/:site_key/*path" => "api/folders#index", constraints: { path: /.*/ }
  post "mkdir/:site_key/*path" => "api/folders#create", constraints: { path: /.*/ }

  # Redirects - compatibility layer
  get "folders/:site_key/*path", to: redirect("%{site_key}/folders/%{path}"), constraints: { path: /.*/ }
  get "folders/:site_key", to: redirect("%{site_key}/folders"), constraints: { path: /.*/ }
  get "files/:site_key/*path", to: redirect("%{site_key}/files/%{path}"), constraints: { path: /.*/ }


  get ":site_key/folders/*path" => "folders#show", as: :folder, constraints: { path: /.*/ }
  get ":site_key/folders" => "folders#show"
  get "no_access_error/" => "folders#no_access_error"

  get ":site_key/files/*path" => "files#show", as: :file, constraints: { path: /.*/ }
  # post 'show/:path' => 'files#show', as: :file
  post ":site_key/files/*path" => "files#create", as: :upload_file, constraints: { path: /.*/ }
  post ":site_key/files" => "files#create"
  delete ":site_key/files/*path" => "files#delete", as: :delete_file, constraints: { path: /.*/ }
  put ":site_key/files/*path" => "files#update", as: :update_file, constraints: { path: /.*/ }
  post ":site_key/folders/*path" => "folders#create", as: :create_folder, constraints: { path: /.*/ }
  post ":site_key/folders" => "folders#create"

  get "help/:category" => "help#show", as: :help, constraints: { path: /api/ }

  # Nagios XML Monitoring POST endpoint
  post "monitoring/health_data"

  # Exception pages
  match "/404" => "errors#not_found", via: [ :get, :post, :delete, :put ]
  match "/422" => "errors#server_error", via: [ :get, :post, :delete, :put ]
  match "/500" => "errors#server_error", via: [ :get, :post, :delete, :put ]
end
