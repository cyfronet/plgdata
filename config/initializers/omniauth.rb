# We have only one way to login that is why we are not affected by
# CVE-2015-9284
OmniAuth.config.allowed_request_methods = [ :get ]
OmniAuth.config.silence_get_warning = true
