Rack::Attack.throttle("requests by proxy", limit: 1, period: 1) do |request|
  request.path if request.path.starts_with? "/upload"
end
