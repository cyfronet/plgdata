expire_after = Rails.env.development? ? 24.hours : 40.minutes

Plgdata::Application.config.session_store :cookie_store,
  key: "_plgdata_session", expire_after:
