Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins "*"
    resource "/download/*", headers: :any, methods: :get
    resource "/list/*", headers: :any, methods: :get
    resource "/upload/*", headers: :any, methods: :post
    resource "/remove/*", headers: :any, methods: :delete
    resource "/mkdir/*", headers: :any, methods: :post
  end
end
