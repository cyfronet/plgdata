class Gftp::ClientMock::File < Gftp::ClientMock::Entry
  def dir?
    false
  end

  def rights
    "-#{@rights}"
  end
end
