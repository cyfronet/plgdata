class Gftp::ClientMock::Entry
  attr_accessor :owner, :group, :size, :modification_date, :name
  attr_writer :rights

  def initialize(rights: "rwxr--r--", owner: "john", group: "plgrid",
                 size: 2048, modification_date: "Sep 15 10:24",
                 name: "Directory")
    @rights = rights
    @owner = owner
    @group = group
    @size = size
    @modification_date = modification_date
    @name = name
  end

  def to_json
    { rights:, owner:, group:, size:,
      modification_date:, name:, is_dir: dir? }
  end

  def rights
    raise "should be implemented in descendent class"
  end
end
