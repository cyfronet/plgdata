class Gftp::ClientMock::Dir < Gftp::ClientMock::Entry
  def dir?
    true
  end

  def rights
    "d#{@rights}"
  end
end
