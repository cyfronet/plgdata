class Gftp::ClientMock::StubRegistry
  include Singleton

  attr_accessor :stubs

  def initialize
    reset!
  end

  def reset!
    self.stubs = {}
  end

  def register_stub(**)
    stubs[{ ** }] = Gftp::ClientMock::Stub.new
  end

  def get(**)
    stubs[{ ** }] || raise("Gftp stub not defined for #{{ ** }}")
  end
end
