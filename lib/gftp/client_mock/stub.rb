class Gftp::ClientMock::Stub
  attr_accessor :buffer, :transferred

  def initialize
    @dir_entries = {}
  end

  def add(dir, entry)
    dir = dir.delete_prefix("/")
    @dir_entries[dir] ||= []
    @dir_entries[dir] << entry
    @dir_entries[File.join(dir, entry.name)] ||= [] if entry.dir?
  end

  def exists?(path)
    find(path).present?
  end

  def list(path)
    path = path.delete_prefix("/")
    @dir_entries[path]&.map(&:to_json) || raise("#{path} not registered")
  end

  def mkdir(path, dir_name, recursive: false)
    add(path, Gftp::ClientMock::Dir.new(name: dir_name))
  end

  def rmdir(path)
    remove(path, dir: true)
  end

  def put(path, file, offset: nil); raise "not defined" end

  def rm(path)
    remove(path, dir: false)
  end

  def rename(path, new_name)
    destination_path = File.join File.dirname(path), new_name
    move(path, destination_path)
  end

  def move(from, to)
    from = from.delete_prefix("/")
    to = to.delete_prefix("/")

    entry = remove(from)
    dir = File.dirname(to)
    new_name = File.basename(to)

    entry.name = new_name

    add(dir, entry)
  end

  def chmod(path, rights)
    entry = find(path)
    if /\A((r|-)(w|-)(x|-)){3}\z/ =~ rights && entry
      entry.rights = rights
      true
    else
      false
    end
  end

  def get(path, before_first_chunk: nil, &block); raise "not defined" end

  private
    def find(path)
      path = path.delete_prefix("/")

      dir = File.dirname(path)
      entry_name = File.basename(path)

      (@dir_entries[dir] || []).find { |e| e.name == entry_name }
    end

    def remove(path, dir: nil)
      path = path.delete_prefix("/")

      find(path).tap do
        dir_path = File.dirname(path)
        entry_name = File.basename(path)

        @dir_entries[dir_path] =
          @dir_entries[dir_path]
          .delete_if { |d| d.name == entry_name && (dir == nil || d.dir? == dir) }
      end
    end
end
