module Gftp
  class ClientMock
    require_relative "client_mock/stub_registry"
    require_relative "client_mock/stub"
    require_relative "client_mock/entry"
    require_relative "client_mock/file"
    require_relative "client_mock/dir"

    def self.ssh(host:, port: 22, cert:, key:)
      new(host:, cert:, key:)
    end

    def self.grid(host:, port: 2811, proxy:)
      new(host:, proxy:)
    end

    delegate :exists?, :list, :mkdir, :rmdir, :get, :put, :rm, :rename, :move, :chmod, :transfer,
      to: :@mock

    private
      def initialize(**)
        @mock = Gftp::ClientMock::StubRegistry.instance.get(**)
      end
  end
end
