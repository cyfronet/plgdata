module Gftp
  class Client
    def initialize(channel)
      @channel = channel
    end

    def self.ssh(host:, port: 22, cert:, key:)
      new(GFTP::Channel::Ssh.new(host:, port:, cert:, key:))
    end

    def self.grid(host:, port: 2811, proxy:)
      new(GFTP::Channel::Grid.new(host:, port:, proxy:))
    end

    def exists?(path)
      result = false

      GFTP::Exists.new(@channel).exists(path) do |valid|
        result = valid
      end

      result
    end

    def list(path)
      entries = []

      GFTP::VerboseList.new(@channel).verbose_list(path) do |es|
        # TODO FIXME when VerboseList returns better format
        entries = es.map do |e|
            e.merge(size: e[:size].to_i, name: e[:name].force_encoding("UTF-8"))
          end
          # This is important for path listing as it prevents regexp chars
          # like *,(,)to get in the way
          .reject { |e| [ ".", ".." ].include?(e[:name]) }
      end

      entries
    end

    def mkdir(path, filename, recursive: false)
      new_folder_path = dir_path(path, sanitize_filename(filename))
      gftp = GFTP::Client.new(@channel)
      result = false

      if recursive
        gftp.mkdir!(new_folder_path) { |answer| result = answer }
      else
        gftp.mkdir(new_folder_path) { |answer| result = answer }
      end

      result
    end

    def rmdir(path)
      dir_path = dir_path(path)
      result = false

      GFTP::RmDir.new(@channel).rmdir(dir_path) do |answer|
        result = answer
      end

      Rails.logger.info "Remove directory #{dir_path}, remove status: #{result}"
      result
    end

    def put(path, file, offset: nil)
      filename = sanitize_filename(file.original_filename)
      target_path = join(path, filename)

      if offset
        data = file.read
        GFTP::PartialPut.new(data.size, @channel)
          .partial_put(target_path, data, offset)
      else
        GFTP::Put.new(@channel).put(target_path) do |buf_size|
          [ file.read(buf_size) || "", file.eof? ]
        end
      end

      filename
    end

    def rm(path)
      result = false

      GFTP::Delete.new(@channel).delete(path) do |answer|
        result = answer
      end

      result
    end

    def rename(path, new_name)
      destination_path = File.join File.dirname(path), sanitize_filename(new_name)
      move(path, destination_path)
    end

    def move(from, to)
      result = false

      GFTP::Move.new(@channel).move(from, to) do |answer|
        result = answer
      end

      result
    end

    def transfer(source, destination)
      result = false

      GFTP::Transfer.new(@channel).transfer(source, destination) do |answer|
        result = answer
      end

      result
    end

    def chmod(path, mode)
      result = false

      GFTP::Chmod.new(@channel).chmod(path, to_numerical_mode(mode)) do |answer|
        result = answer
      end

      result
    end

    def get(path, before_first_chunk: nil, &block)
      first_read = true
      GFTP::Get.new(@channel).get(path) do |data|
        before_first_chunk.call if first_read && before_first_chunk
        first_read = false
        block.call(data)
      end
    end

    private
      def dir_path(*paths)
        join(paths).yield_self do |path|
          path.end_with?("/") ? path : File.join(path, "/")
        end
      end

      def join(*paths)
        File.join(paths, "/")
      end

      def sanitize_filename(filename)
        parser = URI::Parser.new
        parser.escape(
          parser.unescape(
            filename.strip[0..256].tr("/", "_").delete("\0")
          )
        )
      end

      def to_numerical_mode(string_mode)
        mode = nil
        string_mode[0..8].chars.each_slice(3) do |set|
          mode = (mode ? mode * 8 : 00)
          mode += 04 if set[0] == "r"
          mode += 02 if set[1] == "w"
          mode += 01 if set[2] == "x"
        end
        mode
      end
  end
end
